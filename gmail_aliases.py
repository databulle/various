import argparse
from validate_email import validate_email
from itertools import chain, combinations

def powerset(iterable):
    """
    Returns all possible combinations of items in a list.
    powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    """
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-e','--email', required=True, default='injunjoe@gmail.com', 
        type=str, help='Base email') 
    args = parser.parse_args()

    # Let's check if this is a valid email address
    if not validate_email(args.email):
        print('You must enter a valid email address.')

    else:
        name,domain = args.email.split('@')
        name = name.replace('.','')
        
        # Where can we put a dot ?
        positions = list(powerset(list(range(1,len(name)))))

        # We'll store the result aliases in this list
        names = list() 

        # Loop through all possible positions
        for i in positions:
            j = list(i)
            n = list(name)
            counter = 0
            for k in j:
                n.insert(k+counter,'.')
                counter += 1
            names.append(''.join(n))

        # Print results
        for name in names:
            print('{}@{}'.format(name,domain))
