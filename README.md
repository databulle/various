# Various

A repository for small random scripts that don't need a repo of their own yet.

Install dependencies:  

```
pip install -r requirements.txt
```

## gmail_aliases.py

A script to generate every possible _alias_ for a given email.  
For example:  
- toto@gmail.com  
- t.oto@gmail.com  
- to.to@gmail.com  
- tot.o@gmail.com  
- t.o.to@gmail.com  
- t.ot.o@gmail.com  
- to.t.o@gmail.com  
- t.o.t.o@gmail.com  

Usage: `python gmail_aliases.py -e example@email.tld`  